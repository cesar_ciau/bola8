<div id="flash-<?php echo h($key) ?>" class=" message-info success alert alert-success alert-dismissible" role="alert"  style="position: absolute; top: 20px; z-index: 2147483647; right: 10px;">
  <button style="color: black !important" type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i>
</button>
  <?php echo h($message) ?>.
</div>