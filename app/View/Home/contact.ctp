<style type="text/css">
    .container-fluid {
        padding-right: 0px;
        padding-left: 0px;
    }
</style>
<section class="wrapper style1 fade-up text-center">
    <div class="inner">
        <h2 class="titles-contact">Contactanos</h2>
        <div class="row contact-inner">
            <div class="col-md-6">
            <?php echo $this->Form->create();?>
                <!-- <form method="post" action="#"> -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="field half first">
                                <label for="name">Nombre</label>
                                <input type="text" name="name" id="name" required="required">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="field half">
                                <label for="email">Email</label>
                                <input type="email" name="email" id="email" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="field">
                                <label for="message">Mensage</label>
                                <textarea name="message" id="message" rows="5" required="required"></textarea>
                            </div>
                        </div>
                    </div>
                    <ul class="actions">
                        <?php
                        $options = array(
                            'label' => 'Enviar',
                            'class' => 'button submit',
                        );
                        echo $this->Form->end($options);
                        ?>
                    </ul>
            </div>
            <div class="col-md-6 content-contact">
                <div class="">
                    <div class="cool-md-12">
                        <div class="row">
                            <h3 class="titles-contact">Dirección</h3>
                            <div class="col-md-12">
                                <span class="p-contact">Boulevard Adolfo López Mateos 367, <br>
                                    Colonia Altamaya, Álvaro Obregón, 01760
                                <br>
                                México, D.F.</span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <h3 class="titles-contact">Email</h3>
                            <div class="col-md-12">
                                <a  class="p-contact"href="#">info@bola8producciones.com</a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <h3 class="titles-contact">Teléfono</h3>
                            <div class="col-md-12">
                                <span class="p-contact">6277 0705</span>
                                <span class="p-contact">5728 4190</span>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <h3 class="titles-contact">Social</h3>
                            <div class="col-md-12">
                                <ul class="icons">
                                    <li><a  href="https://twitter.com/bola8pro" class="fa fa-twitter fa-5 p-contact"><span class="label"></span></a></li>
                                    <li><a href="https://www.facebook.com/pages/Bola-8-producciones/136176276558168?fref=ts" class="fa fa-facebook fa-5 p-contact"><span class="label"></span></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- <ul class="contact">
                            <li>
                                <h3 class="titles-contact">Dirección</h3>
                                <span class="p-contact">Boulevard Adolfo López Mateos 367, <br>
                                    Colonia Altamaya, Álvaro Obregón, 01760
                                <br>
                                México, D.F.</span>
                            </li>
                            <li>
                                <h3 class="titles-contact">Email</h3>
                                <a  class="p-contact"href="#">info@bola8producciones.com</a>
                            </li>
                            <li>
                                <h3 class="titles-contact">Teléfono</h3>
                                <span class="p-contact">6277 0705</span>
                                <span class="p-contact">5728 4190</span>
                            </li>
                            <li>
                                <h3 class="titles-contact">Social</h3>
                                <ul class="icons">
                                    <li><a  href="https://twitter.com/bola8pro" class="fa fa-twitter fa-5 p-contact"><span class="label"></span></a></li>
                                    <li><a href="https://www.facebook.com/pages/Bola-8-producciones/136176276558168?fref=ts" class="fa fa-facebook fa-5 p-contact"><span class="label"></span></a></li>
                                </ul>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="msj-done" class=" message-info success alert alert-success alert-dismissible hidden" role="alert"  style="position: absolute; top: 20px; z-index: 2147483647; right: 10px;">
  <button style="color: black !important" type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i>
</button>
  El correo fue enviado, gracias por comunicarte con nosotros
</div>
<div id="msj-error" class=" message-info success alert alert-warning alert-dismissible hidden " role="alert"  style="position: absolute; top: 20px; z-index: 2147483647; right: 10px;">
  <button style="color: black !important" type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times" aria-hidden="true"></i>
</button>
  Ocurrió un error al enviar el correo por favor intente mas tarde.
</div>