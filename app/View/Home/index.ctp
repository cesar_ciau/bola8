<div class="row titles-content">
    <div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 title-description">
        <h2>MÁS QUE UNA PRODUCTORA, UNA FAMILIA.</h2>
        <p>Un equipo profesional comprometido con los contenidos audiovisuales
        Más que una casa productora, una familia. Bienvenido a la familia.</p>
    </div>
    <div class="col-md-4 text-center twitter-content visible-lg-block">
        <div class="twitter-header">
            <label>TWITTER</label>
        </div>
        <a class="twitter-timeline" data-chrome="noheader,nofooter" data-lang="es" height="180" href="https://twitter.com/bola8pro">Tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
</div>
<!-- <h1>BOLA 8</h1>
<h2>Mas que una productora, una familia.</h2> -->
<div class="row gallery-conent">
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="animacion-y-graficos">
            <img class="img-gallery" src="img/p_animacion_graficos.jpg" alt="">
        </a>
    </div>
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="nuestros-canales">
            <img class="img-gallery" src="img/p_canales.jpg" alt="">
        </a>
    </div>
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="servicios-corporativos">
            <img class="img-gallery" src="img/p_eventos.jpg" alt="">
        </a>
    </div>
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="plataformas">
            <img class="img-gallery" src="img/p_plataformas.jpg" alt="">
        </a>
    </div>
    <div class="clearfix visible-xs-block"></div>
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="post-produccion">
            <img class="img-gallery" src="img/p_posproduccion.jpg" alt="">
        </a>
    </div>
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="produccion">
            <img class="img-gallery" src="img/p_produccion.jpg" alt="">
        </a>
    </div>
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="shows">
            <img class="img-gallery" src="img/p_show.jpg" alt="">
        </a>
    </div>
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-6 gallery-item">
        <a href="soluciones-creativas">
            <img class="img-gallery" src="img/p_soluciones_creativas.jpg" alt="">
        </a>
    </div>
</div>
<div class="row titles-content hidden-md-twitter">
    <div class="col-md-12 text-center twitter-content">
        <div class="twitter-header">
            <label>TWITTER</label>
        </div>
        <a class="twitter-timeline" data-chrome="noheader,nofooter" data-lang="es" height="180" href="https://twitter.com/bola8pro">Tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
</div>
