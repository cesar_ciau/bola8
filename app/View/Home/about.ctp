<div class="row text-center">
    <div class="col-lg-8 col-md-12 col-xs-12 col-sm-12 section-title" style="padding-left: 0px">
        <div class="section-content">
          <h1>NUESTRO EQUIPO</h1>
          <p>Conozca al equipo que da vida a <strong>Bola 8</strong>.</p>
        </div>
    </div>
    <div class="col-md-4 text-center twitter-content twitter-contact  visible-lg-block">
        <div class="twitter-header">
            <label>TWITTER</label>
        </div>
        <a class="twitter-timeline" data-chrome="noheader,nofooter" data-lang="es" height="180" href="https://twitter.com/bola8pro">Tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
  <!-- <div class="col-md-4 section-service ">
    <div class="section-content red">
      <h2>NOSOTROS</h2>
    </div>
  </div> -->
</div>
<div class="row gallery-conent">
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Eduardo-Suarez-Director-General.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Eduardo Suarez<br> <small>Director General</small></p>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Mauricio-Castillo-Director.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Mauricio Castillo<br> <small>Director</small></p>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Yahir-Vega-Director-Operativo.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Yahir Vega<br> <small>Director Operativo</small></p>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Antonio-Valdes-Director-Operativo.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Antonio Valdes<br> <small>Director Operativo</small></p>
        </div>
    </div>
    <div class="clearfix visible-xs-block"></div>
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Miguel-Villa.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Miguel Villa<br> <small>Director Creativo y de Realizacións</small></p>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Virginia-Ramirez-Directora-dep.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Virginia Ramirez<br> <small>Directora de Produción</small></p>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Aldo-Massa-Project-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Aldo Massar<br> <small>Project Manager</small></p>
        </div>
    </div>
    <div class="col-sm-6 col-md-6 col-lg-3 gallery-item">
        <a>
            <img class="border-boss" src="img/equipo/Daniel-Herrera-Conductor.jpg" alt="">
        </a>
        <div class="mask">
          <p class="text-boss">Daniel Herrera<br> <small>Conductor</small></p>
        </div>
    </div>
    <div class="clearfix visible-xs-block"></div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Alberto-de-la-Oca-Community-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Alberto de la Oca<br> <small>Community Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Alonso-Fajardo.JPG" alt="">
        </a>
        <div class="mask">
          <p>Alonso Fajardo<br> <small>Redacción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Andres-Rosales-Editor.jpg" alt="">
        </a>
        <div class="mask">
          <p>Andres Rosales<br> <small>Editor</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Charbeli-Ramos-Project-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Charbeli Ramos<br> <small>Project Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Christian-Bello.jpg" alt="">
        </a>
        <div class="mask">
          <p>Christian Bello<br> <small>Coordinador de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Cynthia-Bermudez.JPG" alt="">
        </a>
        <div class="mask">
          <p>Cynthia Bermudez<br> <small>Coordinadora Artística</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/David-Cabrera.jpg" alt="">
        </a>
        <div class="mask">
          <p>David Cabrera<br> <small>Redacción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Dexter-Petrelli-Copy.jpg" alt="">
        </a>
        <div class="mask">
          <p>Dexter Petrelli<br> <small>Copy</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Eduardo-Carrasco-Postproductor.jpg" alt="">
        </a>
        <div class="mask">
          <p>Eduardo Carrasco<br> <small>Postproductor</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Edwin-Montes.jpg" alt="">
        </a>
        <div class="mask">
          <p>Edwin Montes<br> <small>Asistente de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Elida-Coronado.jpg" alt="">
        </a>
        <div class="mask">
          <p>Elida Coronado<br> <small>Coordinadora Artística</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Fernanda-Amezcua.jpg" alt="">
        </a>
        <div class="mask">
          <p>Fernanda Amezcua<br> <small>Diseñadora Gráfica</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Gaby-Reyes.jpg" alt="">
        </a>
        <div class="mask">
          <p>Gaby Reyes<br> <small>Coordinadora Artística</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Ingrid-Suarez.jpg" alt="">
        </a>
        <div class="mask">
          <p>Ingrid Suarez<br> <small>Coordinadora de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Irune-Campos-Community-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Irune Campos<br> <small>Community Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Isaac-Mares.JPG" alt="">
        </a>
        <div class="mask">
          <p>Isaac Mares<br> <small>Coordinador de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Isaac-Tapia.jpg" alt="">
        </a>
        <div class="mask">
          <p>Isaac Tapia<br> <small>Redacción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Jessica-Luna-PR-Que-Rollo-Banda.jpg" alt="">
        </a>
        <div class="mask">
          <p>Jessica Luna<br> <small>PR Qué Rollo Banda</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Jimena-Ortega-Project-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Jimena Ortega<br> <small>Project Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Jomi-Ponce-de-Leon-Project-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Jomi Ponce de Leon<br> <small>Project Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Juan-Carlos-Villarreal-Director-de-Nuevos-Negocios.jpg" alt="">
        </a>
        <div class="mask">
          <p>Juan Carlos Villarreal<br> <small>Director de Nuevos Negocios</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Karen-Lamk-Gerente-Creativo.jpg" alt="">
        </a>
        <div class="mask">
          <p>Karen Lamk<br> <small>Gerente Creativo</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Levith-Vega-Editor.jpg" alt="">
        </a>
        <div class="mask">
          <p>Levith Vega<br> <small>Editor</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Luis-Guzman-Conductor.jpg" alt="">
        </a>
        <div class="mask">
          <p>Luis Guzman<br> <small>Conductor</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Mariana-Salas-Project-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Mariana Salas<br> <small>Project Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Mauricio-Gomez.jpg" alt="">
        </a>
        <div class="mask">
          <p>Mauricio Gomez<br> <small>Jefe de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Mauricio-Martinez.jpg" alt="">
        </a>
        <div class="mask">
          <p>Mauricio Martinez<br> <small>Asistente de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Mildred-Carmona-Postproductora.jpg" alt="">
        </a>
        <div class="mask">
          <p>Mildred Carmona<br> <small>Postproductora</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Oliver-Diaz.jpg" alt="">
        </a>
        <div class="mask">
          <p>Oliver Diaz<br> <small>Redacción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Paola-Contreras-PR-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Paola Contreras<br> <small>PR Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Paulina-Ramírez-Project-Manager.jpg" alt="">
        </a>
        <div class="mask">
          <p>Paulina Ramírez<br> <small>Project Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Priscila-Barcelo-Copy.jpg" alt="">
        </a>
        <div class="mask">
          <p>Priscila Barcelo<br> <small>Project Manager</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Rafa-Davila.jpg" alt="">
        </a>
        <div class="mask">
          <p>Rafa Davila<br> <small>Asistente de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Rodrigo-Ramirez.jpg" alt="">
        </a>
        <div class="mask">
          <p>Rodrigo Ramirez<br> <small>Producción Musical</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Samuel-Peñalva-Musicalizacion.jpg" alt="">
        </a>
        <div class="mask">
          <p>Samuel Peñalva<br> <small>Musicalización</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Santiago-VonRaesfeld-Redacción.jpg" alt="">
        </a>
        <div class="mask">
          <p>Santiago VonRaesfeld<br> <small>Redacción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Selene-Cortes-Asistente-de-Producción.jpg" alt="">
        </a>
        <div class="mask">
          <p>Selene Cortes<br> <small>Asistente de Producción</small></p>
        </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-2 gallery-item">
        <a>
            <img class="" src="img/equipo/Vanessa-Zermeño-Coordinador-de-Producción.jpeg" alt="">
        </a>
        <div class="mask">
          <p>Vanessa Zermeño<br> <small>Coordinador de Producción</small></p>
        </div>
    </div>
</div>
<div class="row titles-content hidden-md-twitter">
    <div class="col-md-12 text-center twitter-content">
        <div class="twitter-header">
            <label>TWITTER</label>
        </div>
        <a class="twitter-timeline" data-chrome="noheader,nofooter" data-lang="es" height="180" href="https://twitter.com/bola8pro">Tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
    </div>
</div>