<div class="row no-margin text-center">
    <div class="col-md-8 section-title">
        <div class="section-content">
            <h1>PORTALES</h1>
            <p>Dos portales autosuficientes que cubren toda la información musical que buscas.</p>
        </div>
    </div>
    <div class="col-md-4 section-service ">
        <a href="<?php echo $this->Html->url(["controller"=>"home"])?>">
           <div class="section-content red link-service">
                <h2>SERVICIOS</h2>
            </div>
        </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>Festify</h2>
        <p>En esta plataforma encontrarás mucho contenido relacionado a la gran experiencia que es estar en un festival de música, así como entrevistas espacio para Grupos y solistas independientes, con entrevistas y acústicos desde el foro de Festify.</p>
        <ul  class="list-services">
            <li>Sitio: <a href="http://www.festify.tv">www.festify.tv</a></li>
            <li>YouTube: Festify Tv</li>
            <li>Twitter @FestifyTV</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177576382" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Que Rollo Banda</h2>
        <p>El mundo grupero está lleno de bandas con videos que tienen millones de reproducciones en YouTube.Ahora Sabrás ¿Qué Rollo con ellos? ¿Qué hacen, dónde se presentan?</p>
        <p>Aquí buscamos a cada banda, los entrevistamos y mantenemos la información día a día. Es un espacio para los segidores de intérpretes de norteño, cumbia, salsa, solistas con o sin sombrero, merengue, banda, duranguense.Rodeos, palenques, conciertos y festivales regionales. Todo eso vive en Que Rollo Banda. </p>
        <ul  class="list-services">
            <li>Sitio: <a href="http://www.querollobanda.com">www.querollobanda.com</a></li>
            <li>Twitter: @queRolloBanda</li>
            <li>Facebook: Que Rollo Banda</li>
        </ul>
    </div>
    <div class="col-md-6 col-sm-6">
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Que Rollo Banda</h2>
        <p>El mundo grupero está lleno de bandas con videos que tienen millones de reproducciones en YouTube.Ahora Sabrás ¿Qué Rollo con ellos? ¿Qué hacen, dónde se presentan?</p>
        <p>Aquí buscamos a cada banda, los entrevistamos y mantenemos la información día a día. Es un espacio para los segidores de intérpretes de norteño, cumbia, salsa, solistas con o sin sombrero, merengue, banda, duranguense.Rodeos, palenques, conciertos y festivales regionales. Todo eso vive en Que Rollo Banda. </p>
        <ul  class="list-services">
            <li>Sitio: <a href="http://www.querollobanda.com">www.querollobanda.com</a></li>
            <li>Twitter: @queRolloBanda</li>
            <li>Facebook: Que Rollo Banda</li>
        </ul>
    </div>
</div>
<br>
<br>