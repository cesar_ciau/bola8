<div class="row no-margin text-center">
    <div class="col-md-8 section-title">
        <div class="section-content">
            <h1>SHOWS</h1>
        </div>
    </div>
    <div class="col-md-4 section-service ">
        <a href="<?php echo $this->Html->url(["controller"=>"home"])?>">
           <div class="section-content red link-service">
                <h2>SERVICIOS</h2>
            </div>
        </a>
    </div>
</div>
<br>
<br>
<div>
    <div class="col-md-6 col-sm-6">
        <h2>Show Mauricio Castillo “Hasta que el miembro nos separe”</h2>
    </div>
    <div class="col-md-6 col-sm-6">
    </div>
</div>
<br>
<br>
<div>
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Conferencia “La vida es una película y tiene un final feliz” </h2>
    </div>
    <div class="col-md-6 col-sm-6">
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Conferencia “La vida es una película y tiene un final feliz” </h2>
    </div>
</div>
<br>
<br>