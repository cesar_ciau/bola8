<div class="row no-margin text-center">
    <div class="col-md-8 section-title">
        <div class="section-content">
            <h1>NUESTROS CANALES</h1>
            <p>Éstos son los canales de <strong>bola 8</strong>, con contenidos 100% originales.</p>
        </div>
    </div>
    <div class="col-md-4 section-service ">
        <a href="<?php echo $this->Html->url(["controller"=>"home"])?>">
           <div class="section-content red link-service">
                <h2>SERVICIOS</h2>
            </div>
        </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>La vida es Güera</h2>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177311561" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Sexoral</h2>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177460521" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Sexoral</h2>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>Consejos para mama dolores</h2>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178389243" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Fashionade</h2>
    </div>
    <div class="col-md-6 col-sm-6">
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Fashionade</h2>
    </div>
</div>
<br>
<br>