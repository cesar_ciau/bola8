<div class="row no-margin text-center">
    <div class="col-md-8 section-title">
        <div class="section-content">
            <h1>PRODUCCIÓN  </h1>
        </div>
    </div>
    <div class="col-md-4 section-service ">
        <a href="<?php echo $this->Html->url(["controller"=>"home"])?>">
           <div class="section-content red link-service">
                <h2>SERVICIOS</h2>
            </div>
        </a>
    </div>
</div>

<!-- <p>Post-producción, animación y producción musical.</p> -->
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>PROGRAMA TV “Miembros al aire”</h2>
        <p>Producción del programa de TV Unicable, Televisa Networks desde 2013.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177446335" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>VIDEOCLIP “LOS PANTUFLA”</h2>
        <p>CONCEPTO CREATIVO, PRODUCCIÓN, GRABACION, EDICIÓN, POST PRODUCCIÓN Y PRODUCCIÓN MUSICAL DE Videoclips “Los Pantufla”, 2016.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178320709" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>VIDEOCLIP “LOS PANTUFLA”</h2>
        <p>CONCEPTO CREATIVO, PRODUCCIÓN, GRABACION, EDICIÓN, POST PRODUCCIÓN Y PRODUCCIÓN MUSICAL DE Videoclips “Los Pantufla”, 2016.</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>VIDEOCLIP “RADIO KAOS”</h2>
        <p>CONCEPTO CREATIVO, PRODUCCIÓN, GRABACION, EDICIÓN Y POST PRODUCCIÓN DE Videoclip “Radio KAOS”, 2016.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178101527" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Aeropostale</h2>
        <p>CONCEPTO Y PRODUCCIÓN DEL CONCIERTO 1er ANIVERSARIO AEROPOSTALE. MEXICO, 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177445475" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Aeropostale</h2>
        <p>CONCEPTO Y PRODUCCIÓN DEL CONCIERTO 1er ANIVERSARIO AEROPOSTALE. MEXICO, 2015</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>FUTBOLITO BIMBO 2015</h2>
        <p>CONCEPTO ORIGINAL PARA VIDEOBLOGS Y COBERTURA PARTIDOS “FUTBOLITO BIMBO 2015”, producción, grabación, edición y post-producción Y producción musical para Grupo Bimbo, 2015.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178225420" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>FAISENINGS PEUGEOT</h2>
        <p>propuesta musical para Peugeot conducida por Faisy, en el que un happening llegaba a distintos puntos de la república mexicana, acompañado de una entrevista dentro del Peugeot por las carreteras de México.<br>Grabación, edición, postproducción y producción musical para PEUGEOT MEXICO, 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/176808526" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>FAISENINGS PEUGEOT</h2>
        <p>propuesta musical para Peugeot conducida por Faisy, en el que un happening llegaba a distintos puntos de la república mexicana, acompañado de una entrevista dentro del Peugeot por las carreteras de México.<br>Grabación, edición, postproducción y producción musical para PEUGEOT MEXICO, 2015</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>PROGRAMA TV “FAISENINGS”</h2>
        <p>Programa de televisión conducido por Faisy, en el que un happening musical llegaba a distintos puntos de la república mexicana, con estrellas top de la música en español.<br>Grabación, edición, postproducción y producción musical para RITMOSÓN, TELEVISA NETWORKS 2015.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177311490" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>PORGRAMA TV “Que Rollo” </h2>
        <p>Producción del programa de TV para BANDAMAX, Televisa Networks desde 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178045275" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>PORGRAMA TV “Que Rollo” </h2>
        <p>Producción del programa de TV para BANDAMAX, Televisa Networks desde 2015</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>SKETCHES programa TV “ADAL EL SHOW”</h2>
        <p>Realización, edición y postproducción de sketches en locación para el Programa “adal El Show”, producido por Televisa, 2016</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178101524" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>SKETCHES programa tv “gudnite”</h2>
        <p>GRABACióN, edición, postproducción y producción musical de sketches para el Programa “GUDNITE”, producido por Televisa, 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178043094" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>SKETCHES programa tv “gudnite”</h2>
        <p>GRABACióN, edición, postproducción y producción musical de sketches para el Programa “GUDNITE”, producido por Televisa, 2015</p>
    </div>
</div>
<br>
<br>