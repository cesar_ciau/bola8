<div class="row no-margin text-center">
    <div class="col-md-8 section-title">
        <div class="section-content">
            <h1>POST-PRODUCCIÓN </h1>
            <p>Post-producción, animación y producción musical.</p>
        </div>
    </div>
    <div class="col-md-4 section-service ">
        <a href="<?php echo $this->Html->url(["controller"=>"home"])?>">
           <div class="section-content red link-service">
                <h2>SERVICIOS</h2>
            </div>
        </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>Aeropostale</h2>
        <p>Post-producción, animación y producción musical de la videomemoria concierto 1er aniversario “AEROPOSTALE”.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177445475" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Hard Rock</h2>
        <p>Post-producción, animación y producción musical videomemorias, testigos y conciertos de HardRock, 2015, 2016.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178101525" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Hard Rock</h2>
        <p>Post-producción, animación y producción musical videomemorias, testigos y conciertos de HardRock, 2015, 2016.</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>Corona Sunset</h2>
        <p>COBERTURA FESTIVAL CORONA SUNSET 2015.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178045904" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>SKETCHES PROGRAMA TV ADAL EL SHOW</h2>
        <p>Post-producción, animación y EDICIÓN DE SKETCHES PARA EL PROGRAMA DE TELEVISIÓN “Adal el Show”. para Televisa, 2016.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178042957" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>SKETCHES PROGRAMA TV ADAL EL SHOW</h2>
        <p>Post-producción, animación y EDICIÓN DE SKETCHES PARA EL PROGRAMA DE TELEVISIÓN “Adal el Show”. para Televisa, 2016.</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>video generación 2016 “Colegio olinca”</h2>
        <p>concepto, grabacion, edicion y postproducción de video generación “Colegio Olinca”  Graduación 2016.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/172660429" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>PROMOCIONALES PROGRAMA “Adal El Show” </h2>
        <p>animación Y POSTPRODUCCION  promocionales campaña preventive PROGRAMA “ADAL EL SHOW”, TELEVISA, 2016</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/176808565" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>PROMOCIONALES PROGRAMA “Adal El Show” </h2>
        <p>animación Y POSTPRODUCCION  promocionales campaña preventive PROGRAMA “ADAL EL SHOW”, TELEVISA, 2016</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>ENTRADA PROGRAMA TV “Gudnite”</h2>
        <p>postproducción y Animación entrada institucional programa TV “gudnite” para Televisa, 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178082221" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>spot kinder “Colegio olinca” </h2>
        <p>concepto, grabacion, edicion y postproducción de spot para colegio Olinca. </p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177446413" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>spot kinder “Colegio olinca” </h2>
        <p>concepto, grabacion, edicion y postproducción de spot para colegio Olinca. </p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>ENTRADA PROGRAMA TV “Miembros al aire” </h2>
        <p>Postproduccion y Animación entrada institucional programa TV para Televisa, 2014</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177446335" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Mediacces, spots comerciales campaña pre20</h2>
        <p>PENDIETNE</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178225418" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Mediacces, spots comerciales campaña pre20</h2>
        <p>PENDIETNE</p>
    </div>
</div>
<br>
<br>