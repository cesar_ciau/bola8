<div class="row no-margin text-center">
    <div class="col-md-8 section-title">
        <div class="section-content">
            <h1>Animación y Gráficos</h1>
            <p>A continuacion te mostramos algunos titulos de nuestros trabajos realizados.</p>
        </div>
    </div>
    <div class="col-md-4 section-service ">
        <a href="<?php echo $this->Html->url(["controller"=>"home"])?>">
           <div class="section-content red link-service">
                <h2>SERVICIOS</h2>
            </div>
        </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>Entrada programa “Recuerda y Gana”</h2>
        <p>POST-PRODUCCIÓN Y ANIMACIÓN DE LA ENTRADA INSTITUCIONAL. PRODUCCIÓN PARA CANAL 2, TELEVISA 2016</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178101523" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Visuales para gira Alejandra Guzmán</h2>
        <p>CREACIÓN Y ANIMACIÓN DE VISUALES PARA GIRA ALEJANDRA GUZMÁN, 2016</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178389242" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Visuales para gira Alejandra Guzmán</h2>
        <p>CREACIÓN Y ANIMACIÓN DE VISUALES PARA GIRA ALEJANDRA GUZMÁN, 2016</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>Campaña preventiva y promocionales del programa “Adal El Show”</h2>
        <p>POST PRODUCCIÓN, ANIMACIÓN Y PRODUCCIÓN MUSICAL DE PROMOCIONALES. PRODUCCIÓN PARA CANAL CANAL DE LAS ESTRELLAS, TELEVISA 2016</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178389240" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>Entrada programa “Gudnite” </h2>
        <p>GRABACIÓN, POST-PRODUCCIÓN, PRODUCCIÓN MUSICAL DE LA ENTRADA DEL PROGRAMA “GUDNITE”. PRODUCCIÓN PARA CANAL 5, TELEVISA 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178044137" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Entrada programa “Gudnite” </h2>
        <p>GRABACIÓN, POST-PRODUCCIÓN, PRODUCCIÓN MUSICAL DE LA ENTRADA DEL PROGRAMA “GUDNITE”. PRODUCCIÓN PARA CANAL 5, TELEVISA 2015</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>Entrada programa “Que Rollo” </h2>
        <p>GRABACIÓN, POST-PRODUCCIÓN, ANIMACIÓN Y PRODUCCIÓN MUSICAL DE LA ENTRADA DEL PROGRAMA “QUE ROLLO”.  PARA BANDAMAX/ TELEVISA NETWORKS 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178044203" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="visible-xs-block">
        <div class="col-md-6 col-sm-6">
            <h2>Entrada programa “Miembros al Aire”</h2>
            <p>GRABACIÓN, POST-PRODUCCIÓN Y ANIMACIÓN DE LA ENTRADA DEL PROGRAMA “MIEMBROS AL AIRE”. PRODUCCIÓN PARA UNICABLE/ TELEVISA NETWORKS, UNICABLE, 2014</p>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178389241" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>Entrada programa “Miembros al Aire”</h2>
        <p>GRABACIÓN, POST-PRODUCCIÓN Y ANIMACIÓN DE LA ENTRADA DEL PROGRAMA “MIEMBROS AL AIRE”. PRODUCCIÓN PARA UNICABLE/ TELEVISA NETWORKS, UNICABLE, 2014</p>
    </div>
</div>
<br>
<br>
