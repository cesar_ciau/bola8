<div class="row no-margin text-center">
    <div class="col-md-8 section-title">
        <div class="section-content">
            <h1>SERVICIOS CORPORATIVOS</h1>
            <p>Servicio de STREAMING, Organización y Coordinación de Eventos, Conceptos y Happenings.</p>
        </div>
    </div>
    <div class="col-md-4 section-service ">
        <a href="<?php echo $this->Html->url(["controller"=>"home"])?>">
           <div class="section-content red link-service">
                <h2>SERVICIOS</h2>
            </div>
        </a>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>NESTLÉ</h2>
        <p>Concepto celebración 150 años, 2016</p>
    </div>
    <div class="col-md-6 col-sm-6">
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>KIA</h2>
        <p>Servicio de streaming para la inauguración de KIA México , 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/176813494" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>KIA</h2>
        <p>Servicio de streaming para la inauguración de KIA México , 2015</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>FUTBOLITO BIMBO</h2>
        <p>Cobertura del evento en las diferentes plazas. Diseño de concepto de videoblogs y programa especial TV abierta.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178101526" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>AEROPOSTALE</h2>
        <p>Concierto aniversario Aeropostale 2015, concepto, organización y producción del evento.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177445475" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>AEROPOSTALE</h2>
        <p>Concierto aniversario Aeropostale 2015, concepto, organización y producción del evento.</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>PEPSI</h2>
        <p>Cobertura SuperBolw 2015 en redes sociales.</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178389244" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>VOLKSWAGEN</h2>
        <p>Servicio de Streaming evento autódromo, 2016</p>
    </div>
    <div class="col-md-6 col-sm-6">
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>VOLKSWAGEN</h2>
        <p>Servicio de Streaming evento autódromo, 2016</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>COLEGIO OLINCA</h2>
        <p>Videoclip Kinder, 2015</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177446413" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>DANUP</h2>
        <p>Dirección de cámaras y Servicio de Streaming Premios Danup, 2014</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177445602" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>DANUP</h2>
        <p>Dirección de cámaras y Servicio de Streaming Premios Danup, 2014</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>AEROMÉXICO</h2>
        <p>Spot patrocinio Ricardo Pérez de Lara F1, 2014</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/178389245" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6 visible-xs-block">
        <h2>MASTER CARD</h2>
        <p>Concierto Moderatto, 2014</p>
    </div>
    <div class="col-md-6 col-sm-6">
    </div>
    <div class="col-md-6 col-sm-6 hidden-xs">
        <h2>MASTER CARD</h2>
        <p>Concierto Moderatto, 2014</p>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <h2>MÉXICO BICENTENARIO</h2>
        <p>Dirección de Cámaras transmisión internacional “México Bicentenario” para instantia SA, 2010</p>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/177445573" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
    </div>
</div>
<br>
<br>