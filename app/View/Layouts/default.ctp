<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<link rel="shortcut icon" type="image/x-icon" href="img/Bola8-57x57.png">
	<link rel="apple-touch-icon" href="img/Bola8-57x57.png"/>

	<?php
		// echo $this->Html->meta('icon');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

		echo $this->Html->css('theme');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('font-awesome.min');
		// echo $this->Html->css('theme');
		echo $this->Html->css('main');
		echo $this->Html->css('fonts');
	?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,900">
	<!-- <link href="https://fonts.googleapis.com/css?family=Lato:400,900i" rel="stylesheet"> -->
</head>
<body>
	<div id="main" class="m-scene">
        <div class="hidden-lg">
            <nav class="navbar navbar-default navbar-fixed-top" style="z-index: 9999999999">
              <div class="container-fluid">
                <div class="navbar-header logo-menu-container">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <?php
                  	echo $this->Html->link(
    					$this->Html->image("logo.png", ["alt" => "Brand","class"=>"logo-menu"]),
    						"/",
    						['escape' => false]
					);
					?>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <ul class="nav navbar-nav">
                    <li class="<?php echo ($active == 'index')?'active':''?>"><a href="home">Inicio<span class="sr-only">(current)</span></a></li>
                    <li class="<?php echo ($active == 'about')?'active':''?> "><a href="sobre">Nosotros</a></li>
                    <li class="<?php echo ($active == 'contact')?'active':''?> "><a href="contacto">Contacto</a></li>
                  </ul>
                </div>
              </div>
            </nav>
        </div>
        <div class="col-md-3 no-padding visible-lg-block">
	        <header class="page-header-nav page-header--homepage scene-nav">
	            <nav class="page-nav">
	                <h1 id="fh5co-logo">
	                <?php
                  	echo $this->Html->link(
    					$this->Html->image("logo.png", ["alt" => "bola8-logo","width"=>"220px"]),
    						"/",
    						['escape' => false]
					);
					?>
	                </h1>
	                <ul class="page-nav__list">
	                    <li class="page-nav__item <?php echo ($active == 'index')?'is-active':''?>"><a href="home" class="page-nav__link <?php echo ($active == 'index')?'is-active':''?>">Inicio</a>
	                    </li>
	                    <li class="page-nav__item <?php echo ($active == 'about')?'is-active':''?>"><a href="sobre" class="page-nav__link <?php echo ($active == 'about')?'is-active':''?>">Nosotros</a></li>
	                    <li class="page-nav__item <?php echo ($active == 'contact')?'is-active':''?>"><a href="contacto" class="page-nav__link <?php echo ($active == 'contact')?'is-active':''?>">Contacto</a></li>
	                </ul>
	            </nav>
	        </header>
            <div class="sidebar-footer hidden-small">
            	<div class="row">
            		<div class="col-md-4 text-center">
						<a class="links-social" href="https://www.facebook.com/pages/Bola-8-producciones/136176276558168?fref=ts" >
							<span class="fa fa-facebook fa-5" aria-hidden="true"></span>
						</a>
            		</div>
            		<div class="col-md-4 text-center">
            			<a class="links-social" href="https://twitter.com/bola8pro">
							<span class="fa fa-twitter fa-5" aria-hidden="true"></span>
						</a>
            		</div>
            		<div class="col-md-4 text-center">
            			<a class="links-social" href="#">
							<span class="fa fa-instagram fa-5" aria-hidden="true"></span>
						</a>
            		</div>
            	</div>
              <!-- <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
                <span class="fa fa-facebook fa-5" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a> -->
            </div>
        </div>
        <div class="col-lg-9 col-md-12 col-xs-12 no-padding <?php echo (isset($style_content))?$style_content:''?>">
	        <div class="page-content scene-main scene-main--fadein sceneElement">
	        	<div class="container-fluid">
          <?php echo $this->Flash->render(); ?>
					<?php echo $this->Flash->render('positive');?>
					<?php echo $this->fetch('content'); ?>
	        	</div>
	        </div>
        </div>
	</div>
	<div class="footer-mobile hidden-small hidden-lg">
    	<div class="row">
    		<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 text-center">
				<a class="links-social" href="https://www.facebook.com/pages/Bola-8-producciones/136176276558168?fref=ts">
					<span class="fa fa-facebook fa-5" aria-hidden="true"></span>Facebook
				</a>
    		</div>
    		<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 text-center">
    			<a class="links-social" href="https://twitter.com/bola8pro">
					<span class="fa fa-twitter fa-5" aria-hidden="true"></span> Twitter
				</a>
    		</div>
    		<div class="col-md-4 col-xs-4 col-sm-4 col-lg-4 text-center">
    			<a class="links-social" href="#">
					<span class="fa fa-instagram fa-5" aria-hidden="true"></span> Instagram
				</a>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12 text-center">
    			<p>Copyright 2015 Bola 8 Producciones. Todos los derechos reservados.</p>
    		</div>
    	</div>
    </div>
	<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
	<?php echo $this->Html->script('jquery.smoothState.min');?>
	<?php echo $this->Html->script('main');?>
	<?php echo $this->Html->script('packages');?>
	<?php echo $this->Html->script('theme');?>
	<?php echo $this->Html->script('bootstrap.min');?>
</body>
</html>
