(function ($) {

    'use strict';

    $(document).ready(function () {

        // Init here.
        var sendForm = function(){
            $("#contactForm").on("submit", function (e){
                e.preventDefault();
                e.stopPropagation();
                var data = $(this).serializeArray();
                $.ajax({
                    url: '/bola8/contacto',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                })
                .done(function(data) {
                    if (data.valid) {
                        $("#msj-done").removeClass('hidden');
                    } else {
                        $("#msj-error").removeClass('hidden');
                    }
                })
                .fail(function(data) {
                    alert("A ocurrido un error al enviar el correo por favor intente mas tarde");
                    console.log("error");
                });
            })
        }
        sendForm();
        var $body = $('body'),
            $main = $('#main'),
            $site = $('html, body'),
            transition = 'fade',
            smoothState;

        smoothState = $main.smoothState({
            onStart: {
                duration: 400,
                render: function (url, $container) {
                    $main.attr('data-transition', transition);
                    $main.addClass('is-exiting');
                    $site.animate({scrollTop: 0});
                    smoothState.restartCSSAnimations();
                    theme();
                    sendForm();
                }
            },
            onReady: {
                duration: 0,
                render: function ($container, $newContent) {
                    $container.html($newContent);
                    $container.removeClass('is-exiting');
                    theme();
                    sendForm();
                }
            },
            allowFormCaching: true,
            form:'contactForm'
        }).data('smoothState');

    });

}(jQuery));