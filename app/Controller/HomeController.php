<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class HomeController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
	// public $components = array('Security');

	// public function initialize()
	// {
	//     parent::initialize();
	//     $this->loadComponent('Csrf');
	// }


	public function index()
	{
		$style_content = "";
		$title_for_layout = "Productora Bola 8";
		$active = "index";
		$this->set(compact('title_for_layout', 'active', 'style_content'));
	}

	public function about()
	{
		$style_content = "";
		$title_for_layout = "Sobre Bola 8";
		$active = "about";
		$this->set(compact('title_for_layout', 'active', 'style_content'));
	}

	public function contact()
	{
		if (!empty($this->request->data)) {
			$this->autoRender = false;
			$data = $this->request->data;
			// $this->Flash->success('El correo fue enviado, gracias por comunicarte con nosotros', array('key' => 'positive'));
			$text = "La persona ".$data["name"].", quiere ponerse en Contacto con usted.<br>";
			$text .= "El correo de la persona es ".$data["email"].". <br>";
			$text .= "Mando el siguiente mensaje '".$data["message"]."'.";
			$Email = new CakeEmail();
			$Email->from(array('bola8@site.com' => 'Contacto Bola 8'));
			$Email->to('cesarfhs@gmail.com');
			$Email->subject('Contacto de Prospecto');
			if ($Email->send($text)) {
				echo json_encode(array("valid"=>true));
			} else {
				echo json_encode(array("valid"=>false));
			}
	      }
		$style_content = "padding-contact";
		$title_for_layout = "Contacto Bola 8";
		$active = "contact";
		$this->set(compact('title_for_layout', 'active','style_content'));
	}
}
