<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class SectionsController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

	public function animacion()
	{
		$active = "index";
		$title_for_layout = "Animación y Gráficos";
		$this->set(compact('title_for_layout', 'active'));
	}
	public function canales()
	{
		$active = "index";
		$title_for_layout = "Canales Bola 8";
		$this->set(compact('title_for_layout', 'active'));
	}
	public function eventos()
	{
		$active = "index";
		$title_for_layout = "Servicios Corporativos ";
		$this->set(compact('title_for_layout', 'active'));
	}
	public function plataformas()
	{
		$active = "index";
		$title_for_layout = "Portales";
		$this->set(compact('title_for_layout', 'active'));
	}

	public function postproduccion()
	{
		$active = "index";
		$title_for_layout = "POST-PRODUCCIÓN";
		$this->set(compact('title_for_layout', 'active'));
	}

	public function produccion()
	{
		$active = "index";
		$title_for_layout = "PRODUCCIÓN";
		$this->set(compact('title_for_layout', 'active'));
	}

	public function shows()
	{
		$active = "index";
		$title_for_layout = "SHOWS";
		$this->set(compact('title_for_layout', 'active'));
	}

	public function soluciones()
	{
		$active = "index";
		$title_for_layout = "SOLUCIONES CREATIVAS";
		$this->set(compact('title_for_layout', 'active'));
	}

}
